angular.module('app.recursiveForm', ['ngRoute'])

.config(['$routeProvider', function($routeProvider, $mdDateLocaleProvider) {
	$routeProvider.when('/recursive-form', {templateUrl: 'view/recursive-form/recursive-form.html'});
}])

.controller('addDiaryCtrl', ['$scope', '$http', '$route',
					   function($scope, $http, $route) {





			// Recursive JSON
			$scope.aletrsList = 	{
										"idEventInfo":"", 
										"alerts": 	[
														{
															"idEventStatus": [],		
															"idMomentDay": "",
															"datesBeforeEndDate": "",
															"datesBeforeStartDate": ""		
														}
													]
									};

			$scope.addAlert = function() {
				$scope.aletrsList.alerts.push({
												"idEventStatus": [],		
												"idMomentDay": "",
												"datesBeforeEndDate": "",
												"datesBeforeStartDate": ""		
											});
			};

			$scope.removeAlert = function(key) {
				if ($scope.aletrsList.alerts.length > 1) {
					$scope.aletrsList.alerts.pop({
													"idEventStatus": [],		
													"idMomentDay": "",
													"datesBeforeEndDate": "",
													"datesBeforeStartDate": ""	
												});
				} else {
					$scope.aletrsList.alerts[0] = undefined;
				}
			};


			$scope.pushStatus = function(key, check, valueStatus) {
				// Key = ID de formulario recursivo
				// check = El modelo del check que esta clickeando el usuario, devuelve true - false
				// valueStatus = Es el valor que se tiene que pushear, en este caso es el ID del objeto que se pasa para construir los checks
				if ($scope.aletrsList.alerts[key].idEventStatus.length === 0) {
					$scope.aletrsList.alerts[key].idEventStatus.push(valueStatus);
				} else {
					if (check === true) {
						$scope.aletrsList.alerts[key].idEventStatus.push(valueStatus);
					} else {
						angular.forEach($scope.aletrsList.alerts[key].idEventStatus, function(value, keyF) {
							if (value === valueStatus) {
								$scope.aletrsList.alerts[key].idEventStatus.splice(keyF, 1);
								return
							} 
						});
					}
				}
			};



}]);